﻿using PersonSpace;

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

using var db = new PersonContext();

// Note: This sample requires the database to be created before running.
Console.WriteLine($"Database path: {db}.");

// Create
Console.WriteLine("Inserting a new Person");
db.Add(new PersonModel { Name = "Max" });
db.SaveChanges();

// Read
Console.WriteLine("Querying for a blog");
var PersonList = db.Persons
    .OrderBy(b => b.Name);

foreach (var item in PersonList)
{
    Console.WriteLine(item.Name);
}
