﻿namespace PersonSpace;
using System.ComponentModel.DataAnnotations;
public class PersonModel
{
    [Key]
    public int Id {get; set;}
    public required string Name {get; set;}
}
