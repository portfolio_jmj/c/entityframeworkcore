namespace PersonSpace;
using Microsoft.EntityFrameworkCore;

public class PersonContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to postgres with connection string from app settings
        options.UseNpgsql("Host=localhost:5432;Username=postgres;Password=postgres;Database=postgres");
    }

    public DbSet<PersonModel> Persons { get; set; }
}