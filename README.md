## Nuget 
`dotnet add DbMapping/Person/Person.csproj package Microsoft.EntityFrameworkCore --version 8.0.0`

`dotnet add DbMapping/Person/Person.csproj package Microsoft.EntityFrameworkCore.Design`

`dotnet add DbMapping/Person/Person.csproj  package Npgsql.EntityFrameworkCore`


## Migration
`dotnet tool install --global dotnet-ef`

`dotnet ef --project DbMapping/Person/Person.csproj migrations add InitialCreate`

`dotnet ef --project DbMapping/Person/Person.csproj database update`

